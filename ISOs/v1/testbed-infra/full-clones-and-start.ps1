﻿# --------------------------------------

# created by ascherr // 12/3/2019
# PURPOSE: VM full clone via vcenter and start VM after deploy
# --------------------------------------

# authenticate to single VC and disable confirmation
Set-PowerCLIConfiguration -DefaultVIServerMode single -Confirm:$false -Scope Session -Verbose

# ---- VALUES which must be set 
#        ------ BEGIN -----
$MASTERVM = "ISTAv1-template" # MASTER-VM original
$NEWVMNAME = "ISTAv1-loadgen"     # VM TestName
$DATASTORENAME = "vsanDatastore" # vsan datastore name
$RESSOURCE = "ISTAloadgen"  # resource name as destination
$AMOUNTofVMS = "10"         # how many VMs
$CLUSTERNAME = "vSAN67"
$VCIP = "10.10.10.1"           # VCenter IP-address
$username = "Administrator@vsphere.local"  # Username to authenticate against VCenter
$userpwd = "YourPassword"          # Password to user
#        ------ END -----

# login into vcenter 
Write-Verbose -Message "---------------------------------------------------------------"

$VCENTERFOLDER=(Get-VM $MASTERVM).Folder

# check if resource pool for VM cloning exists
$checkres = Get-ResourcePool -Name $RESSOURCE -InformationAction SilentlyContinue
if (!$checkres) { 
New-ResourcePool -Location $CLUSTERNAME -Name $RESSOURCE 
}

$a = 0001
DO {
 "Starting to create full-clone # $a"
New-VM -Name $NEWVMNAME-$a -VM $MASTERVM -ResourcePool $RESSOURCE -Datastore $DATASTORENAME |Out-Null
$a++
} While ($a -le $AMOUNTofVMS)

## starting all VMs deployed
$a = 0001
DO {
Start-VM -vm $NEWVMNAME-$a  -Confirm:$false -RunAsync |Out-Null
$a++
} While ($a -le $AMOUNTofVMS)
