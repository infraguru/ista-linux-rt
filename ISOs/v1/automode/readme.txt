# workflow process
# easiest using contentlib and add entry, upload iso image
# create a VM via VC or ESXi webUI
# choose Linux-others x64 or any other x64 bit linux as OS
# make sure to use "BIOS" option, not efi
# attach as by choosing either iso on datastore or iso via contentlib
# add as many disks you need on the VM
# clone the VM with all the settings via powercli or other method as often you need the VM
# disks will be initialized on the first boot
# after disk initialization, VM shuts down
# after all VMs are offline, for the test you start all VMs at the same time for the test
# runtime is mentioned below and VM shuts down with easyrun again, when completed.
# to repeat, just starting VMs again
# in all the tests, monitor stats in any form its applicable 


# ---------------------------------
# istav112RT-x86_64-automode-variable-profile-longrun.iso
# ---------------------------------
# runtime 600sec each test
# iotests through loop the different runs against all attached disks
# blocksize="4k 16k 64k 256k"
# iodepth="4 16"
# reads="100 70 50 30 0"
# randomness="100"
# size="100%"  # workingset
# split into 4 disk parts for testing for randomness

#-------------------------------------------
# istav112RT-x86_64-auto-easyrun-4k-70r-4OIO-random-all-disks.iso
# ------------------------------------------

# automatic mode - easy run against all attached disks
# runtime 10800sec, workload 70% read, 30% writes with 4k blocksize, OIO=4, full random
# blocksize="4k"
# iodepth="4"
# reads="70"
# randomness="100"
# size="100%"  # workingset
# split into 4 disk parts for testing for randomness


# istav112RT-x86_64-auto-4k-1M-70r-random-unaligned-all-disks.iso
# ------------------------------------------
# runtime="10800"
# filejobs="1"
# blocksizerange="4k-1024k"
# unaligned block random choice
# iodepth="4"
# reads="70"
# randomness="100"
# size="100%"  # workingset
# split into 4 disk parts for testing for randomness


