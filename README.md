# ISTAv2-alpha04
- build from buildroot, with updated packages for gcc 10.1, fio, stress-ng, iperf3, netperf
- Linux Kernel 5.6.10 with realtime extension, no kernel modules (compiled-in)
- boot from ISO into ramdisk, 256MB minimum required with 1vCPU 
- stress-ng vers. 0.11.10
- fio vers. 3.19
- netperf vers 2.7.0
- iperf vers. 2.0.13
- iperf3 vers 3.7 (cJSON1.5.2)
- buildroot cross-compiler gcc 10.1


# ista-linux-rt - v1
- Buildroot framework based micro linux with busybox
- Linux Kernel 4.19.15 with RT extension, headers 4.19, binutils-2.31-1, GCC 8.2.1 -O3 -full-stack-protection
- Tools build-in: fio 3.13beta, iperf, iperf3, netperf, stress-ng
- ISO & bzImage+efi stub for testing CPU/network/storage performance in VMW VM (bootable cdrom/legacy bios, bzimage + bzimage.efi for pxe boot)
- x86_64 image, no 32bit code, no python, no bash
- Image-aarch64.efi - efi image for AARCH64, pure 64bit
- all efi image with in-kernel efi-stub

# todo manual, use fedora / centos or other linux distribution
- git clone https://github.com/buildroot/buildroot.git
- copy buildroot.config to buildroot/.config
- download kernel and use available RT extension (latest version if possible)
- patch kernel with extension
- copy kernel.config linux-4.19*/.config
- build buildroot and copy to output/images/*.cpio ../
- change cpio cfilename if necessary or if other changes added, example additional files added configs (unpack/repack cpio again)
- compile kernel with make -j4 or -j8, make isoimage

# only for your personal testing. -- no support or guarantees --
# Please visit legal and package license legal-info.tgz provided by the buildroot package 

